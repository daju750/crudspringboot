package crud.spring.boot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import crud.spring.boot.entity.Estudiante;
import crud.spring.boot.service.EstudianteServicio;
import crud.spring.boot.form.FormEstudiante;

import crud.spring.boot.component.conversiones;

@Controller
@RequestMapping({"/", "/inicio"})
public class inicio {
	
	@Autowired
	private EstudianteServicio estudianteServ;
	
	@GetMapping
    public ModelAndView getindex(){

		ModelAndView modelandview = new ModelAndView("index");
		modelandview.addObject("FormEstudiante",new FormEstudiante());
		modelandview.addObject("ListaEstudiante",this.estudianteServ.ConsultarTodo());
    	return modelandview;
    }
	
	@PostMapping
    public ModelAndView postindex(FormEstudiante formEstudiante){
		ModelAndView modelandview = new ModelAndView();
		conversiones conv = new conversiones();
		
		Estudiante estudiante = new Estudiante();
		estudiante.setNombres(formEstudiante.getNombres());
		estudiante.setApellidos(formEstudiante.getApellidos());
		estudiante.setFechaNacimiento(conv.StringFechaDate(formEstudiante.getFechaNacimiento()));
		estudiante.setDireccion(formEstudiante.getDireccion());
		this.estudianteServ.Guardar(estudiante);
		
		modelandview.setViewName("redirect:/");

    	return modelandview;
    }
	
	@RequestMapping("/eliminar/{id}")
    public ModelAndView eliminar(@PathVariable("id") long id){
		
		ModelAndView modelandview = new ModelAndView();
		this.estudianteServ.Eliminar(id);
		modelandview.setViewName("redirect:/");
		
		return  modelandview;
    }
	
	@GetMapping
	@RequestMapping("/editar/{id}")
    public ModelAndView geteditar(Model model,@PathVariable Long id){
		
		Estudiante estudiante = this.estudianteServ.Consulta(id);
		ModelAndView modelandview = new ModelAndView("editar");
		FormEstudiante formEstudiante = new FormEstudiante();
		formEstudiante.setNombres(estudiante.getNombres());
		formEstudiante.setApellidos(estudiante.getApellidos());
		formEstudiante.setDireccion(estudiante.getDireccion());
		String[] cadena = estudiante.getFechaNacimiento().toString().split(" ");
		String[] cadena1 = cadena[0].split("-");
		formEstudiante.setFechaNacimiento(cadena1[2]+"-"+cadena1[1]+"-"+cadena1[0]);
		model.addAttribute("id",id);
		modelandview.addObject("FormEstudiante",formEstudiante);
		modelandview.addObject(model);
		return modelandview;
    }

	@PostMapping
	@RequestMapping("/guardar/{id}")
    public ModelAndView posteditar(FormEstudiante formEstudiante,@PathVariable Long id){
		
		ModelAndView modelandview = new ModelAndView();
		conversiones conv = new conversiones();
		
		Estudiante estudiante = new Estudiante();
		estudiante.setId(id);
		estudiante.setNombres(formEstudiante.getNombres());
		estudiante.setApellidos(formEstudiante.getApellidos());
		estudiante.setDireccion(formEstudiante.getDireccion());
		estudiante.setFechaNacimiento(conv.StringFechaDate(formEstudiante.getFechaNacimiento()));
		this.estudianteServ.Guardar(estudiante);
		modelandview.setViewName("redirect:/");
		
		return modelandview;
    }
	

}
