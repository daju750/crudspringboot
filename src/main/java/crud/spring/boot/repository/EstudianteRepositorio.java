package crud.spring.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import crud.spring.boot.entity.Estudiante;

public interface EstudianteRepositorio extends JpaRepository<Estudiante,Long>{

}
