package crud.spring.boot.entity;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "estudiante")
public class Estudiante {

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Id;
	
	@Column(name= "nombres" , length = 50)
	private String Nombres;
	
	@Column(name= "apellidos", length = 50)
	private String Apellidos;

	@Column(name= "fechaNacimiento")
    private Date FechaNacimiento;
	
	@Column(name= "direccion", length = 50)
	private String Direccion;

	public Estudiante(){}
	
	public Estudiante(Long id, String nombres, String apellidos, Date fechaNacimiento, String direccion) {
		super();
		Id = id;
		Nombres = nombres;
		Apellidos = apellidos;
		FechaNacimiento = fechaNacimiento;
		Direccion = direccion;
	}

	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getNombres() {
		return Nombres;
	}

	public void setNombres(String nombres) {
		Nombres = nombres;
	}

	public String getApellidos() {
		return Apellidos;
	}

	public void setApellidos(String apellidos) {
		Apellidos = apellidos;
	}

	public Date getFechaNacimiento() {
		return FechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		FechaNacimiento = fechaNacimiento;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	@Override
	public int hashCode() {
		return Objects.hash(Apellidos, Direccion, FechaNacimiento, Id, Nombres);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estudiante other = (Estudiante) obj;
		return Objects.equals(Apellidos, other.Apellidos) && Objects.equals(Direccion, other.Direccion)
				&& Objects.equals(FechaNacimiento, other.FechaNacimiento) && Objects.equals(Id, other.Id)
				&& Objects.equals(Nombres, other.Nombres);
	}

	@Override
	public String toString() {
		return "Estudiante [Id=" + Id + ", Nombres=" + Nombres + ", Apellidos=" + Apellidos + ", FechaNacimiento="
				+ FechaNacimiento + ", Direccion=" + Direccion + "]";
	}
	
}
