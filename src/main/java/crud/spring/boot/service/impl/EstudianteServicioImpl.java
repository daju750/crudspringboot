package crud.spring.boot.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import crud.spring.boot.entity.Estudiante;
import crud.spring.boot.service.EstudianteServicio;
import crud.spring.boot.repository.EstudianteRepositorio;

@Service
public class EstudianteServicioImpl implements EstudianteServicio{

	@Autowired
	private EstudianteRepositorio estudianteRepo;
	
	@Override
	@Transactional
	public void Guardar(Estudiante estudiante) {
		this.estudianteRepo.save(estudiante);
	}

	@Override
	@Transactional
	public List<Estudiante> ConsultarTodo() {
		return this.estudianteRepo.findAll();
	}

	@Override
	@Transactional
	public void Eliminar(Long id) {
		this.estudianteRepo.deleteById(id);
	}

	@Override
	@Transactional
	public Estudiante Consulta(Long id) {
		Estudiante estudiante = new Estudiante();
		Optional<Estudiante> op = this.estudianteRepo.findById(id);
		estudiante.setNombres(op.get().getNombres());
		estudiante.setApellidos(op.get().getApellidos());
		estudiante.setDireccion(op.get().getDireccion());
		estudiante.setFechaNacimiento(op.get().getFechaNacimiento());
		return  estudiante;
	}

}
