package crud.spring.boot.service;

import java.util.List;
import crud.spring.boot.entity.Estudiante;

public interface EstudianteServicio {

	public void Guardar(Estudiante estudiante);
	public  List<Estudiante> ConsultarTodo();
	public void Eliminar(Long id);
	public Estudiante Consulta(Long id);
	
}
